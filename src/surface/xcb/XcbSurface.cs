﻿using oovulkan.device;
using oovulkan.instance;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.surface.xcb
{
    public unsafe class XcbSurface : Surface
    {
        public readonly XcbConnectionHandle Connection;
        public readonly XcbWindowHandle Window;
        public readonly XcbVisualHandle Visual;

        internal XcbSurface(Device device, XcbConnectionHandle connection, XcbWindowHandle window, XcbVisualHandle visual)
            : base(device)
        {
            Connection = connection;
            Window = window;
            Visual = visual;
            Create();
        }

        private void Create()
        {
            #region Validation
            FilterQueueFamilies(family => family.SupportsXcb(Connection, Visual));
            #endregion

            var info = new XcbSurfaceCreateInfo(Connection, Window);
            using(Device.WeakLock)
            {
                SurfaceHandle handle;
                using(Device.PhysicalDevice.Instance.WeakLock)
                using(Device.Allocator?.WeakLock)
                    vkCreateXcbSurfaceKHR(Device.PhysicalDevice.Instance, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }

        private readonly VkCreateXcbSurfaceKHR vkCreateXcbSurfaceKHR;

        private delegate Result VkCreateXcbSurfaceKHR(InstanceHandle instance, XcbSurfaceCreateInfo* info, AllocationCallbacks* allocator, out SurfaceHandle surface);
    }

    public static class XcbSurfaceHelper
    {
        public static XcbSurface GetXcbSurface(this Device device, XcbConnectionHandle connection, XcbWindowHandle window, XcbVisualHandle visual)
            => new XcbSurface(device, connection, window, visual);
    }
}