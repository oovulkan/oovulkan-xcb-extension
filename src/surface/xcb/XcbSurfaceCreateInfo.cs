﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.surface.xcb
{
    [Flags]
    internal enum XcbSurfaceCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct XcbSurfaceCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly XcbSurfaceCreateFlags Flags;
        public readonly XcbConnectionHandle Connection;
        public readonly XcbWindowHandle Window;

        public XcbSurfaceCreateInfo(XcbConnectionHandle connection, XcbWindowHandle window)
        {
            Type = StructureType.XlibSurfaceCreateInfoKhr;
            Next = null;
            Flags = XcbSurfaceCreateFlags.None;
            Connection = connection;
            Window = window;
        }
    }
}